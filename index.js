// console.log('Hello, World');

let posts = [];
let count = 1;

document.querySelector('#form-add-post').addEventListener('submit', event => {
	// preventDefault - stops the auto reload of the webpage when submitting forms
	event.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#text-body').value
	})

	console.log(posts);
	count++;
	showPosts(posts);
})

const showPosts = posts => {
	let postEntries = '';

	posts.forEach(post => {
		postEntries += `<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onclick="editPost('${post.id}')">Edit</button>
		<button onclick="deletePost('${post.id}')">Delete</button>
		<hr>
		</div>`
	})

	console.log(postEntries);

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit post
const editPost = id => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#text-edit-body').value = body;
	document.querySelector('#txt-edit-id').value = id;
	// console.log(title);
	// console.log(body);
}

document.querySelector('#form-edit-post').addEventListener('submit', event => {
	event.preventDefault();

	const idToBeEdited = document.querySelector('#txt-edit-id').value;
	// console.log(idToBeEdited);

	for(let i = 0; i < posts.length; i++) {
		if(posts[i].id.toString() === idToBeEdited) {
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#text-edit-body').value;

			showPosts(posts);
			alert('Edit is successful!');

			break;
		}
	}
})

const deletePost = id => {
	// document.querySelector(`#post-${id}`).addEventListener('submit', event => {
	// 	posts.splice(id - 1, 1);
	// })
	// document.querySelector(`#post-${id}`).remove();

	posts.splice(id - 1, 1);
	const result = confirm('Are you sure you want to delete this post?')
	if(result) {
		document.querySelector(`#post-${id}`).remove();
	} 
}
